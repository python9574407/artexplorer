import tkinter as tk
from tkinter import ttk
from PIL import Image, ImageTk
import firebase_admin
from firebase_admin import credentials, firestore

class OrderHistoryApp:
    def __init__(self, root):
        self.root = root
        root.title("Order History")
        root.configure(bg="#89CFF3")
        screen_width = root.winfo_screenwidth()
        screen_height = root.winfo_screenheight()
        root.geometry(f"{screen_width}x{screen_height}+0+0")

        cred = credentials.Certificate("artexplorer\\artexplorer-fa168-firebase-adminsdk-q5zil-bb3fe1310e.json")  
        firebase_admin.initialize_app(cred)
        self.db = firestore.client()  




        self.setup_widgets()

    def fetch_orders(self):
        orders = []
        
        orders_ref = self.db.collection('orders')
        for order_doc in orders_ref.stream():
            order_data = order_doc.to_dict()
            orders.append(order_data)
        return orders

    def setup_widgets(self):
        self.history_frame = tk.Frame(self.root, bg="#89CFF3")
        self.history_frame.pack(fill=tk.BOTH, expand=False)
        orders = self.fetch_orders()
        row = 0
        col = 0
        for index, order in enumerate(orders):
            if index % 4 == 0 and index != 0:
                row += 1
                col = 0
            order_frame = tk.Frame(self.history_frame, bg="#A0E9FF")
            order_frame.grid(row=row, column=col, padx=50, pady=10, sticky="nsew")
            col += 1
            image_path = order.get("image_path", "")
            if image_path:
                image = Image.open(image_path)
                image = image.resize((100, 100), Image.ADAPTIVE)
                photo = ImageTk.PhotoImage(image)
                image_label = tk.Label(order_frame, image=photo, bg="#A0E9FF")
                image_label.image = photo
                image_label.pack()
            order_label = tk.Label(order_frame, text=f"Order {order['order_id']}:\n Date: {order['date']},\n Total: ${order['total']}", bg="#A0E9FF")
            order_label.pack()

def main():
    root = tk.Tk()
    app = OrderHistoryApp(root)
    root.mainloop()

if __name__ == "__main__":
    main()
