from tkinter import *
from tkinter import messagebox, Toplevel
from google.cloud import firestore
import json
from PIL import Image, ImageTk
import webbrowser
from ttkthemes import ThemedStyle
credentials_path = 'E:\\ArtExplorer\\artexplorer\\artexplorer-fa168-firebase-adminsdk-q5zil-bb3fe1310e.json'
with open(credentials_path) as json_file:
    credential_info = json.load(json_file)
db = firestore.Client.from_service_account_info(credential_info)

class Main:
    def __init__(self):
        self.root = Tk()
        self.root.title("Signup Page")
        self.root.attributes('-fullscreen', True)  
        self.root.bind("<Escape>", lambda event: self.root.attributes('-fullscreen', False))
        self.root.config(bg="#89CFF3")

       
        bg_image = Image.open("artexplorer\\assets\\bgimg.png")
        bg_image = bg_image.resize((self.root.winfo_screenwidth(), self.root.winfo_screenheight()), resample=Image.BICUBIC)
        self.background_image = ImageTk.PhotoImage(bg_image)

        
        self.background_label = Label(self.root, image=self.background_image)
        self.background_label.place(x=0, y=0, relwidth=1, relheight=1)

        self.frame = Frame(self.background_label, width=500, height=600, bg="#E5D4FF", borderwidth=10, relief="groove", highlightbackground="#00008B")
        self.frame.place(relx=0.5, rely=0.5, anchor=CENTER)
        self.menubar()

    def menubar(self):
        width = self.root.winfo_screenwidth()
        self.menu_frame = Frame(self.root, width=width, height=70, bg="#DCBFFF")
        self.logo = Label(self.menu_frame, text="ArtExplorer", bg="#DCBFFF", font=("Segoe Script",35), fg="#114303")
        self.home = Button(text="  Home  ", bg="#DCBFFF", fg="blue", bd=0, font=("arial,17"))
        self.about = Button(text="About us", bg="#DCBFFF", fg="blue", bd=0, font=("arial,17"), command=self.about_us)
        self.team = Button(text="KnowMore", bg="#DCBFFF", fg="blue", bd=0, font=("arial,17"), command=self.open_website)
        self.logo.place(relx=0.05, rely=0)
        self.home.place(relx=0.75, y=8)
        self.about.place(relx=0.81, y=8)
        self.team.place(relx=0.88, y=8)
        self.menu_frame.pack()

    def about_us(self):
        about_window = Toplevel(self.root)
        about_window.title("About Us")
        self.root.attributes('-fullscreen', True)  
        self.root.bind("<Escape>", lambda event: self.root.attributes('-fullscreen', False))
        about_window.configure(bg="#CDF5FD")
        about_image_path = "artexplorer\\assets\\aboutus.jpg"
        about_image = Image.open(about_image_path)
        about_image = about_image.resize((600, 600), resample=Image.BICUBIC)  
        
        about_image = ImageTk.PhotoImage(about_image)
        about_image_label = Label(about_window, image=about_image, bg="#CDF5FD")
        about_image_label.image = about_image  
        about_image_label.pack()

        about_window.mainloop()
        
    def open_website(self):
        webbrowser.open("https://www.core2web.in/")

    def signup(self):
        self.frame.destroy()
        self.frame = Frame(self.root, width=500, height=600, bg="#CDF5FD", borderwidth=10, relief="groove", highlightbackground="#00008B")
        self.frame.place(relx=0.5, rely=0.5, anchor=CENTER)
        self.gender_entry = StringVar()
        self.gender_entry.set('male')
        self.heading = Label(self.frame, text="Sign Up", font=("arial greek",30,"bold"), bg="#CDF5FD")
        self.fname_label = Label(self.frame, text="First name", bg="#CDF5FD", font=("arial",17))
        self.fname_entry = Entry(self.frame, font=("arial",15))
        self.fname_entry.config(highlightthickness=1, highlightbackground="#00008B", relief="groove")
        self.lname_label = Label(self.frame, text="Last name", bg="#CDF5FD", font=("arial",17))
        self.lname_entry = Entry(self.frame, font=("arial",15))
        self.lname_entry.config(highlightthickness=1, highlightbackground="#00008B", relief="groove")
        self.age_label = Label(self.frame, text="Age", bg="#CDF5FD", font=("arial",17))
        self.age_entry = Entry(self.frame, font=("arial",12))
        self.age_entry.config(highlightthickness=1, highlightbackground="#00008B", relief="groove")
        self.email_label = Label(self.frame, text="Email", bg="#CDF5FD", font=("arial",17))
        self.email_entry = Entry(self.frame, font=("arial",12))
        self.email_entry.config(highlightthickness=1, highlightbackground="#00008B", relief="groove")
        self.gender_label = Label(self.frame, text="Gender", bg="#CDF5FD", font=("arial",17))
        self.raido1 = Radiobutton(self.frame, text="Male", variable=self.gender_entry, command="choice", value='male', bg="#CDF5FD", font=("arial",15))
        self.raido2 = Radiobutton(self.frame, text="Female", variable=self.gender_entry, command="choice", value='female', bg="#CDF5FD", font=("arial",15))
        # gender_entry=ttk.Combobox(frame,values=['Male','Female'],font=("arial",12))
        # gender_entry.set('Male')
        self.mobile_label = Label(self.frame, text="Mobile no.", bg="#CDF5FD", font=("arial",17))
        self.mobile_entry = Entry(self.frame, font=("arial",12))
        self.mobile_entry.config(highlightthickness=1, highlightbackground="#00008B", relief="groove")
        self.create_username_label = Label(self.frame, text="Create Username", bg="#CDF5FD", font=("arial",17))
        self.create_username_entry = Entry(self.frame, font=("arial",15))
        self.create_username_entry.config(highlightthickness=1, highlightbackground="#00008B", relief="groove")
        self.password_label = Label(self.frame, text="Create Password", bg="#CDF5FD", font=("arial",17))
        self.password_entry = Entry(self.frame, show="*", font=("arial",20))
        self.password_entry.config(highlightthickness=1, highlightbackground="#00008B", relief="groove")
        self.signup_button = Button(self.frame, text="Sign Up", bg="#00A9FF", font=("arial",15), command=self.get_signup_data)
        self.login_label = Label(self.frame, text="already have account ?", bg="#CDF5FD", font=("arial",9))
        self.login_button = Button(self.frame, text="login", bg="#CDF5FD", fg="blue", font=("arial",9), bd=0, command=self.login) 
        self.heading.place(x=175,y=30)
        self.fname_label.place(x=15,y=130)
        self.fname_entry.place(x=140,y=135,width=200,height=27)
        self.lname_label.place(x=15,y=180)
        self.lname_entry.place(x=140,y=185,width=200,height=27)
        self.age_label.place(x=300,y=280)
        self.age_entry.place(x=375,y=285,width=50,height=27)
        self.gender_label.place(x=15,y=230)
        self.raido1.place(x=110,y=235,width=100,height=27)
        self.raido2.place(x=205,y=235,width=100,height=27)
        self.email_label.place(x=15,y=330)
        self.email_entry.place(x=135,y=335,width=300,height=27)
        self.mobile_label.place(x=15,y=280)
        self.mobile_entry.place(x=135,y=285,width=100,height=27)
        self.create_username_label.place(x=40,y=400)
        self.create_username_entry.place(x=240,y=405,width=200,height=27)
        self.password_label.place(x=40,y=440)
        self.password_entry.place(x=240,y=445,width=200,height=27)
        self.signup_button.place(x=200,y=500,width=100,height=40)
        self.login_label.place(x=160,y=545)
        self.login_button.place(x=290,y=545)
        self.frame.place(relx=.5,rely=.55,anchor=CENTER)
        self.root.mainloop()
    

    def Home(self):
        self.root.wm_attributes('-transparentcolor', 'black')
        self.frame.destroy()

        bg_image = Image.open("artexplorer\\assets\\bgimg.png")
        bg_image = bg_image.resize((self.root.winfo_screenwidth(), self.root.winfo_screenheight()), resample=Image.BICUBIC)
        self.background_image = ImageTk.PhotoImage(bg_image)
        self.home_canvas = Canvas(self.root, width=self.root.winfo_screenwidth(), height=self.root.winfo_screenheight(), bg='black')
        self.home_canvas.pack(fill=BOTH, expand=YES)

        
        self.home_canvas.create_image(0, 0, image=self.background_image, anchor=NW)
        self.home_canvas.create_text(750, 200, text="Welcome To Art_Explorer", font=("Segoe Script", 25, "bold"), fill='black')
        self.home_canvas.create_text(750, 300, text="From Years ...", font=("PT serif", 16, "bold"), fill='black')
        self.home_canvas.create_text(750, 340, text="Artists and Buyers have trusted Art-Explorer ", font=("PT serif", 16, "bold"), fill='black')
        self.home_canvas.create_text(750, 380, text="to buy and sell Art .", font=("PT serif", 16, "bold"), fill='black')
        self.home_canvas.create_text(750, 420, text="Now Find Your Perfect Art !!!", font=("PT serif", 16, "bold"), fill='black')
        self.signup_button = Button(self.home_canvas, text="Don't have an account? Register now !", fg="blue", font=("Arial", 9), bd=0, command=self.signup)
        self.home_canvas.create_window(750, 680, window=self.signup_button)

        self.root.mainloop()



    def login(self):
        self.frame.destroy()
        self.frame = Frame(self.root, width=500, height=600, bg="#CDF5FD", borderwidth=10, relief="groove", highlightbackground="#00008B")
        self.frame.place(relx=0.5, rely=0.5, anchor=CENTER)
        style = ThemedStyle(self.root)
        style.theme_use('clam')
        style.configure('TFrame', borderwidth=10, relief='ridge', background="#CDF5FD")

        self.heading = Label(self.frame, text="login", font=("arial greek",30,"bold"), bg="#CDF5FD")
        self.username_label = Label(self.frame, text="Username", bg="#CDF5FD", font=("arial",18))
        self.username_entry = Entry(self.frame, font=("arial",12))
        self.username_entry.config(highlightthickness=1, highlightbackground="#00008B", relief="groove")
        self.password_label = Label(self.frame, text="Password", bg="#CDF5FD", font=("arial",18))
        self.password_entry = Entry(self.frame, show="*", font=("arial",20))
        self.password_entry.config(highlightthickness=1, highlightbackground="#00008B", relief="groove")
        self.login_button = Button(self.frame, text="login", bg="#00A9FF", font=("arial",15), command=self.get_login_data)
        self.signup_label = Label(self.frame, text="not registered ?", bg="#CDF5FD", font=("arial",9))
        self.signup_button = Button(self.frame, text="register now !", bg="#CDF5FD", fg="blue", font=("arial",9), bd=0, command=self.signup) 

        self.heading.place(x=200, y=130)
        self.username_label.place(x=70, y=230)
        self.username_entry.place(x=210, y=235, width=200, height=27)
        self.password_label.place(x=70, y=280)
        self.password_entry.place(x=210, y=285, width=200, height=27)
        self.login_button.place(x=200, y=350, width=100, height=40)
        self.signup_label.place(x=155, y=400)
        self.signup_button.place(x=250, y=400)
        self.frame.place(relx=.5, rely=.55, anchor=CENTER)
        self.root.mainloop()

    def open_portfolio(self):
        from portfolio import PortfolioApp
        portfolio_window = Toplevel()  
        portfolio_window.title("Portfolio")
        portfolio_window.attributes('-fullscreen', True)
        portfolio_window.bind("<Escape>", lambda event: portfolio_window.attributes('-fullscreen', False))
        portfolio_app = PortfolioApp(portfolio_window)
        portfolio_window.mainloop()


    def get_login_data(self):
        self.username = self.username_entry.get()
        self.password = self.password_entry.get()

        user_profiles_ref = db.collection('user_profile')
        user_profiles = user_profiles_ref.stream()
        flag = False

        for user_profile in user_profiles:
            user_data = user_profile.to_dict()
            if self.username == user_data.get('username') and self.password == user_data.get('password'):
                flag = True
                break

        if flag:
            self.open_portfolio()  
        else:
            messagebox.showinfo("Login Failed", "Invalid username or password")

    def get_signup_data(self):
        self.username = self.create_username_entry.get()
        self.first_name = self.fname_entry.get()
        self.last_name = self.lname_entry.get()
        self.age = self.age_entry.get()
        self.gender = self.gender_entry.get()
        self.email = self.email_entry.get()
        self.mobile_no = self.mobile_entry.get()
        self.password = self.password_entry.get()
        try:
            if len(self.first_name) == 0 or len(self.last_name) == 0 or len(self.mobile_no) == 0 or len(self.email) == 0 or len(self.password) == 0 or len(self.username) == 0 or len(self.age) == 0:
                messagebox.showinfo("blank", "please fill complete data")

            else:
                if not self.mobile_no.isdigit() or len(self.mobile_no) != 10:
                    messagebox.showinfo("blank", "please enter valid number")
                else:
                    #database
                    self.user_profile_ref = db.collection('user_profile')
                    self.user1 = {'first_name':self.first_name, 'last_name':self.last_name, 'mobile_no': self.mobile_no, 'gender':self.gender, 'email':self.email, 'username':self.username, 'password':self.password}
                    self.new_user_ref = self.user_profile_ref.add(self.user1)
                    messagebox.showinfo("signup", "Registered Successfully") 
        except:
            messagebox.showinfo("blank", "technical error")

if __name__ == '__main__':
    app = Main()
    app.Home()

