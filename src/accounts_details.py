import tkinter as tk
from tkinter import Frame, simpledialog
from PIL import Image, ImageTk
import firebase_admin
from firebase_admin import credentials, firestore
import portfolio
if not firebase_admin._apps:
    cred = credentials.Certificate("artexplorer\\artexplorer-fa168-firebase-adminsdk-q5zil-bb3fe1310e.json")
    firebase_admin.initialize_app(cred)
    db = firestore.client()
else:
    db = firestore.client()


def authenticate_user(username, password):
    user_ref = db.collection('user_profile').where('username', '==', username).where('password', '==',
                                                                                         password).limit(1)
    users = user_ref.get()
    return len(users) == 1


def get_user_data(user_id):
    user_doc = db.collection('user_profile').document(user_id).get()
    if user_doc.exists:
        return user_doc.to_dict()
    else:
        return None


class accounts_details:
    def __init__(self, root=None, user_id=None, user_data=None, return_callback=None):
        self.root = root
        self.user_id = user_id
        self.user_data = user_data
        self.return_callback = return_callback
        self.username = user_data.get('username')
        if self.username:
            self.setup_gui()
        else:
            print("User not found.")

    def setup_gui(self):
        self.root = tk.Toplevel()
        self.root.title(f"{self.username}'s Account Details")

        # Load and display background image
        bg_image = Image.open("artexplorer\\assets\\bgimg.png") 
        bg_image = bg_image.resize((2000, 1200), resample=Image.BICUBIC)  
        bg_photo = ImageTk.PhotoImage(bg_image)
        bg_label = tk.Label(self.root, image=bg_photo)
        bg_label.image = bg_photo
        bg_label.place(x=0, y=0, relwidth=1, relheight=1)

        screen_width = self.root.winfo_screenwidth()
        screen_height = self.root.winfo_screenheight()
        self.root.geometry(f"{screen_width}x{screen_height}+0+0")
        x_center = (self.root.winfo_screenwidth() - screen_width) // 2
        y_center = (self.root.winfo_screenheight() - 400) // 2
        container = Frame(self.root, highlightthickness=2, highlightbackground="#00A9FF", highlightcolor="black")
        container.configure(bg="#A0E9FF")
        container.pack(pady=(y_center, 0), padx=(x_center, 0), anchor="center")
        header = tk.Label(container, text="Account Details", font=("Arial", 20, "bold"), background="#A0E9FF")
        header.pack(padx=200, pady=20, fill=tk.X)

        self.labels = {}
        exclude_fields = ['username', 'password']
        for key, value in self.user_data.items():
            if key not in exclude_fields:
                label = tk.Label(container, text=f"{key}: {value}", font=("Helvetica", 16), padx=10, pady=5)
                label.configure(bg=("#A0E9FF"))
                label.pack(fill=tk.X)
                self.labels[key] = label

        change_details = tk.Button(container, text="Change Details", font=("Helvetica", 16), fg="black", bg="#00A9FF",
                                    command=self.change_account_details_from_input)
        change_details.pack(padx=10, pady=10)

        go_to_home = tk.Button(container, text="Go to Home", font=("Helvetica", 16), fg="Black", bg="#00A9FF",
                                borderwidth=2, command=self.return_to_portfolio)
        go_to_home.pack(padx=10, pady=10)

    def change_account_details_from_input(self):
        new_account_details = {}
        for key in self.labels.keys():
            new_value = simpledialog.askstring("Change Details", f"Enter new {key.strip(':- ')}", parent=self.root)
            if new_value is not None:
                new_account_details[key] = new_value

        doc_ref = db.collection('user_profile').document(self.user_id)
        doc_ref.update(new_account_details)

        for key, label in self.labels.items():
            if key in new_account_details:
                label.config(text=f"{key}: {new_account_details[key]}")

    def return_to_portfolio(self):
        self.root.destroy()
        if self.return_callback:
            self.return_callback()


def main():
    user_id = "H9q9nufu86gLMW44Y7zz"
    user_data = get_user_data(user_id)
    if user_data:
        root = tk.Tk()
        portfolio_app = portfolio.PortfolioApp(root)
        return_callback = root.deiconify()
        app = accounts_details(user_id=user_id, user_data=user_data, return_callback=return_callback)
        root.withdraw()
        root.mainloop()
    else:
        print("User data not found.")


if __name__ == "__main__":
    main()
