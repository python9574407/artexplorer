import tkinter as tk
from tkinter import messagebox
from PIL import Image, ImageTk
import firebase_admin
from firebase_admin import credentials, firestore

# Initialize Firebase Admin SDK
cred = credentials.Certificate("artexplorer\\artexplorer-fa168-firebase-adminsdk-q5zil-bb3fe1310e.json")  # Replace with your own path
firebase_admin.initialize_app(cred)
db = firestore.client()

def save_payment_info(payment_info):
    db.collection('payments').add(payment_info)

def process_payment():
    global card_number_entry, expiry_entry, name_entry, address_entry, phone_entry
    card_number = card_number_entry.get()
    expiry_date = expiry_entry.get()
    payment_method = payment_method_var.get()

    # Simple validation for card number and expiry date
    if not (card_number and expiry_date):
        messagebox.showerror("Error", "Please fill in all fields.")
        return

    if len(card_number) != 16:
        messagebox.showerror("Error", "Invalid card number. Please enter 16 digits.")
        return

    if len(expiry_date) != 5 or "/" not in expiry_date:
        messagebox.showerror("Error", "Invalid expiry date. Please use the format MM/YY.")
        return

    
    payment_info = {
        'name': name_entry.get(),
        'address': address_entry.get(),
        'phone': phone_entry.get(),
        'card_number': card_number,
        'expiry_date': expiry_date,
        'payment_method': payment_method
    }
    save_payment_info(payment_info)

    
    show_thank_you_window()

def show_thank_you_window():
    thank_you_window = tk.Toplevel()
    thank_you_window.title("Thank You!")
    thank_you_window.configure(bg="#86B6F6")
    thank_you_window.attributes('-fullscreen', True)  
    thank_you_image = Image.open("artexplorer\\assets\\Thankyou.jpg")
    thank_you_image = thank_you_image.resize((600, 400), Image.BICUBIC)
    thank_you_photo = ImageTk.PhotoImage(thank_you_image)
    thank_you_label = tk.Label(thank_you_window, image=thank_you_photo)
    thank_you_label.image = thank_you_photo
    thank_you_label.pack(expand=True)

    
    def back_to_portfolio():
        thank_you_window.destroy()

    back_button = tk.Button(thank_you_window, text="Back to Portfolio", command=back_to_portfolio, bg="green")
    back_button.pack(pady=10)

    thank_you_window.mainloop()


def main():
    global card_number_entry, expiry_entry, payment_method_var, name_entry, address_entry, phone_entry, root

    root = tk.Tk()
    root.title("Payment Page")
    root.configure(bg="#86B6F6")
    root.attributes('-fullscreen', True)  
    root.bind("<Escape>", lambda event: root.attributes('-fullscreen', False))

    bg_image = Image.open("artexplorer\\assets\\bgimg.png")
    bg_image = bg_image.resize((root.winfo_screenwidth(), root.winfo_screenheight()), Image.BICUBIC)
    bg_photo = ImageTk.PhotoImage(bg_image)
    bg_label = tk.Label(root, image=bg_photo)
    bg_label.image = bg_photo
    bg_label.place(x=0, y=0, relwidth=1, relheight=1)

    outer_frame_width = 3000  
    outer_frame_height = 5000  

    frame = tk.Frame(root, bg="#B4D4FF", width=outer_frame_width, height=outer_frame_height)  
    frame.pack(pady=60)

    inner_frame_width = 1500  
    inner_frame_height = 800  

    inner_frame = tk.Frame(frame, bg="#B4D4FF", width=inner_frame_width, height=inner_frame_height) 
    inner_frame.pack(padx=60, pady=60)

    name_label = tk.Label(inner_frame, text="Name:", bg="#B4D4FF", font=("Helvetica", 12, "bold"))
    name_label.grid(row=0, column=0, padx=10, pady=5)
    name_entry = tk.Entry(inner_frame, font=("Helvetica", 12))
    name_entry.grid(row=0, column=1, padx=10, pady=5)

    address_label = tk.Label(inner_frame, text="Address:", bg="#B4D4FF", font=("Helvetica", 12, "bold"))
    address_label.grid(row=1, column=0, padx=10, pady=5)
    address_entry = tk.Entry(inner_frame, font=("Helvetica", 12))
    address_entry.grid(row=1, column=1, padx=10, pady=5)

    phone_label = tk.Label(inner_frame, text="Phone Number:", bg="#B4D4FF", font=("Helvetica", 12, "bold"))
    phone_label.grid(row=2, column=0, padx=10, pady=5)
    phone_entry = tk.Entry(inner_frame, font=("Helvetica", 12))
    phone_entry.grid(row=2, column=1, padx=10, pady=5)

    
    card_number_label = tk.Label(inner_frame, text="Card Number:", bg="#B4D4FF", font=("Helvetica", 12, "bold"))
    card_number_label.grid(row=3, column=0, padx=10, pady=5)
    card_number_entry = tk.Entry(inner_frame, font=("Helvetica", 12))
    card_number_entry.grid(row=3, column=1, padx=10, pady=5)

    expiry_label = tk.Label(inner_frame, text="Expiry Date (MM/YY):", bg="#B4D4FF", font=("Helvetica", 12, "bold"))
    expiry_label.grid(row=4, column=0, padx=10, pady=5)
    expiry_entry = tk.Entry(inner_frame, font=("Helvetica", 12))
    expiry_entry.grid(row=4, column=1, padx=10, pady=5)

    payment_method_label = tk.Label(inner_frame, text="Payment Method:", bg="#B4D4FF", font=("Helvetica", 12, "bold"))
    payment_method_label.grid(row=5, column=0, padx=10, pady=5)

    payment_method_var = tk.StringVar()
    payment_method_var.set("Credit Card") 
    payment_methods = ["Credit Card", "Debit Card", "PayPal"]

    for i, method in enumerate(payment_methods):
        rb = tk.Radiobutton(inner_frame, text=method, variable=payment_method_var, value=method, bg="#B4D4FF", font=("Helvetica", 12))
        rb.grid(row=i + 5, column=1, padx=10, pady=5, sticky="w")

    payment_button = tk.Button(inner_frame, text="Process Payment", command=process_payment, bg="green", font=("Helvetica", 12, "bold"))  
    payment_button.grid(row=len(payment_methods) + 6, columnspan=2, padx=10, pady=10)

    back_button = tk.Button(root, text="Back", command=back_to_main_window, bg="green", font=("Helvetica", 12, "bold"))  
    back_button.pack(side=tk.BOTTOM, pady=20)

    root.mainloop()

def back_to_main_window():
    root.destroy()

if __name__ == "__main__":
    main()
