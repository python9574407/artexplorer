import tkinter as tk
from PIL import Image, ImageTk
import firebase_admin
from firebase_admin import credentials, firestore
import accounts_details
import Login
import subprocess

# Initialize Firebase Admin SDK
if not firebase_admin._apps:
    cred = credentials.Certificate("artexplorer\\artexplorer-fa168-firebase-adminsdk-q5zil-bb3fe1310e.json")
    firebase_admin.initialize_app(cred)
    db = firestore.client()
else:
    db = firestore.client()

class PortfolioApp:
    def __init__(self, master):
        self.master = master
        self.master.title("artExplorer Portfolio")
        self.master.configure(bg="#89CFF3")

        self.header_frame = tk.Frame(self.master, bg="#DCBFFF")
        self.header_frame.grid(row=0, column=0, columnspan=2, sticky="ew")

        self.header_label = tk.Label(self.header_frame, text="Art Explorer", bg="#DCBFFF", fg="#114303", font=("Segoe Script", 35))
        self.header_label.grid(row=0, column=0, padx=10, pady=10)

        # Oval and MyAccount button
        self.canvas = tk.Canvas(self.master, width=40, height=40, bg="#DCBFFF", bd=0, highlightthickness=0)
        self.canvas.create_oval(5, 5, 35, 35, outline="black", fill="#00A9FF")
        self.canvas.grid(row=0, column=1, padx=10, pady=10, sticky="ne")
        self.canvas.bind("<Button-1>", self.toggle_slider)

        # Create a canvas and add a scrollbar
        self.canvas_frame = tk.Frame(self.master)
        self.canvas_frame.grid(row=1, column=0, sticky="nsew")

        self.canvas_scroll = tk.Canvas(self.canvas_frame)
        self.canvas_scroll.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        self.scrollbar = tk.Scrollbar(self.canvas_frame, orient=tk.VERTICAL, command=self.canvas_scroll.yview)
        self.scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
        self.canvas_scroll.configure(yscrollcommand=self.scrollbar.set)

        # Content frame inside canvas
        self.right_frame = tk.Frame(self.canvas_scroll, bg="#CDF5FD")
        self.canvas_scroll.create_window((0, 0), window=self.right_frame, anchor=tk.NW)

        self.master.rowconfigure(1, weight=1)
        self.master.columnconfigure(0, weight=1)

        self.slider_visible = False

        self.fetch_data()
        self.create_widgets()  # Populate frames with fetched data

    def fetch_data(self):
        self.art_data = []
        art_collection = db.collection('art').get()
        for doc in art_collection:
            art_info = doc.to_dict()
            self.art_data.append(art_info)

    def create_widgets(self):
        # Add refresh button
        refresh_button = tk.Button(self.header_frame, text="Refresh", command=self.refresh_screen, bg="#00A9FF")
        refresh_button.grid(row=0, column=1, padx=10, pady=10)

        self.buy_buttons = []  # List to store Buy buttons

        num_columns = 6
        for index, art_info in enumerate(self.art_data):
            row = index // num_columns
            col = index % num_columns
            art_frame = tk.Frame(self.right_frame, bd=2, relief=tk.RAISED, bg="white")
            art_frame.grid(row=row, column=col, padx=5, pady=5, sticky="nsew")

            # Load and display image
            image_path = art_info.get("image_path")
            if image_path:
                image = Image.open(image_path)
                new_size = (200, 200)
                image = image.resize(new_size, resample=Image.BICUBIC)
                photo = ImageTk.PhotoImage(image)
                image_label = tk.Label(art_frame, image=photo, bg="white")
                image_label.image = photo
                image_label.image_path = image_path  # Store the image path as an attribute
                image_label.grid(row=0, column=0, padx=5, pady=5)

                # Bind mouse enter and leave events to the image label
                image_label.bind("<Enter>", lambda event, label=image_label, path=image_path: self.zoom_image(label, path, 250))
                image_label.bind("<Leave>", lambda event, label=image_label, path=image_path: self.zoom_image(label, path, 200))



            # Display artist name
            artist_label = tk.Label(art_frame, text=art_info.get("artist_name", "Unknown Artist"), bg="white")
            artist_label.grid(row=1, column=0, padx=5, pady=5)

            # Display description
            description_label = tk.Label(art_frame, text=art_info.get("description", "Art Description"), bg="white")
            description_label.grid(row=2, column=0, padx=5, pady=5)

            # Display price
            price_label = tk.Label(art_frame, text="Price: " + art_info.get("price", "Rs.XX.XX"), bg="white")
            price_label.grid(row=3, column=0, padx=5, pady=5)

            # Create and add Buy button
            buy_button = tk.Button(art_frame, text="Buy", command=lambda idx=index: self.buy_art(idx), bg="#00A9FF")
            buy_button.grid(row=4, column=0, padx=5, pady=5)
            self.buy_buttons.append(buy_button)  # Add the Buy button to the list

        for i in range(row + 1):
            self.right_frame.grid_rowconfigure(i, weight=1)
        for j in range(num_columns):
            self.right_frame.grid_columnconfigure(j, weight=1)

        # Update the scroll region of the canvas to encompass the entire frame
        self.right_frame.update_idletasks()
        self.canvas_scroll.configure(scrollregion=self.canvas_scroll.bbox("all"))

    def zoom_image(self, label, path, size):
        """
        Function to zoom in/out an image label.

        Parameters:
            label (tk.Label): The image label to zoom.
            path (str): The path of the image associated with the label.
            size (int): The new size of the image label.
        """
        image = Image.open(path)
        image_resized = image.resize((size, size), resample=Image.BICUBIC)
        photo = ImageTk.PhotoImage(image_resized)
        label.config(image=photo)
        label.image = photo

    def toggle_slider(self, event):
        if not self.slider_visible:
            self.show_slider()
        else:
            self.hide_slider()

    def show_slider(self):
        if not self.slider_visible:
            self.slider_frame = tk.Toplevel(self.master)
            self.slider_frame.geometry("150x100")
            self.slider_frame.overrideredirect(True)
            x = self.master.winfo_rootx() + self.canvas.winfo_x() + self.canvas.winfo_width() // 2 - 75
            y = self.master.winfo_rooty() + self.canvas.winfo_y() + self.canvas.winfo_height() + 5
            self.slider_frame.geometry(f"+{x}+{y}")
            buttons = [
                ("MyAccount", self.open_account_details, "#89CFF3"),
                ("Be an Artist", self.open_artist, "#89CFF3"),
                ("Logout", self.logout, "#89CFF3")
            ]
            for button_text, command, bg_color in buttons:
                tk.Button(self.slider_frame, text=button_text, command=command, bg=bg_color).pack(fill=tk.X)

            self.slider_visible = True

    def hide_slider(self):
        if self.slider_frame:
            self.slider_frame.destroy()
            self.slider_visible = False

    def dummy_function(self):
        pass

    def buy_art(self, index):
        art_info = self.art_data[index]

        # Execute payments.py using subprocess
        subprocess.Popen(["python", "artexplorer\\src\\payments.py"])

        # Change Buy button to Purchased
        self.buy_buttons[index].config(text="Purchased", bg="red", state="disabled")

    def open_account_details(self):
        # Fetch user data here
        user_id = "H9q9nufu86gLMW44Y7zz"  # Assuming you have a way to obtain the user_id
        user_data = accounts_details.get_user_data(user_id)
        if user_data:
            # Destroy the slider frame if it's visible
            self.hide_slider()

            if hasattr(self, "account_window") and self.account_window.winfo_exists():
                # If the account window is already open and exists, bring it to the front
                self.account_window.lift()
                self.account_window.focus_force()  # Set focus to the account window
            else:
                # Otherwise, create a new account window
                self.account_window = tk.Toplevel(self.master)
                self.account_window.title("Account Details")
                self.account_window.attributes('-fullscreen', True)
                self.account_window.bind("<Escape>", lambda event: self.account_window.attributes('-fullscreen', False))

                accounts_details_instance = accounts_details.accounts_details(self.account_window, user_id, user_data, self.return_to_portfolio)
                self.account_window.lift()
                self.account_window.focus_force()
        else:
            print("User data not found.")

    def open_artist(self):
        from Artist import ArtistInfo  # Import the ArtistInfo class from Artist.py

        # Create a new Toplevel window for the artist information
        artist_window = tk.Toplevel(self.master)
        artist_window.title("Artist Information")
        artist_window.attributes('-fullscreen', True)
        artist_window.bind("<Escape>", lambda event: artist_window.attributes('-fullscreen', False))

        # Initialize an instance of ArtistInfo with the parent window
        artist_app = ArtistInfo(artist_window)

        # Start the main loop of the artist window
        artist_window.mainloop()

    def logout(self):
        # Destroy the current instance of the portfolio application
        self.master.destroy() 
        
        # Create a new instance of the login page
        login_instance = Login.Main()  
        login_instance.login()

    def return_to_portfolio(self):
        self.master.deiconify()

    def refresh_screen(self):
        # Destroy current widgets
        for widget in self.right_frame.winfo_children():
            widget.destroy()

        # Fetch new data and recreate widgets
        self.fetch_data()
        self.create_widgets()

def main():
    root = tk.Tk()
    app = PortfolioApp(root)
    root.attributes('-fullscreen', True)
    root.bind("<Escape>", lambda event: root.attributes('-fullscreen', False))
    root.mainloop()

if __name__ == "__main__":
    main()
