import tkinter as tk
from tkinter import filedialog
import firebase_admin
from firebase_admin import credentials, firestore
from portfolio import PortfolioApp


if not firebase_admin._apps:
    cred = credentials.Certificate("artexplorer\\artexplorer-fa168-firebase-adminsdk-q5zil-bb3fe1310e.json")  # Update with your service account key path
    firebase_admin.initialize_app(cred)
db = firestore.client()

class ArtistInfo(tk.Toplevel):
    instance = None  
    def __new__(cls, *args, **kwargs):
        if not cls.instance:
            cls.instance = super().__new__(cls)
        return cls.instance

    def __init__(self, parent):
        if hasattr(self, 'initialized'):  
            return
        super().__init__(parent)

        self.initialized = True

        self.title("Header Example")
        self.attributes('-fullscreen', True)  
        self.bind("<Escape>", lambda event: self.attributes('-fullscreen', False))
        self.configure(bg='#CDF5FD')

        header_frame = tk.Frame(self)
        header_frame.pack(fill=tk.X, padx=10, pady=10) 

        self.header_label = tk.Label(header_frame, text="MY ART",bg="#DCBFFF")
        self.header_label.config(font=("Segoe Script",35),fg="#114303")  
        self.header_label.pack(fill=tk.X)  

        button_frame = tk.Frame(self)
        button_frame.pack(side=tk.BOTTOM, pady=10) 

        self.button = tk.Button(button_frame, text="Add Art",bg='#00A9FF', command=self.open_add_art_window)
        self.button.pack() 

        self.history_window = None

        back_button = tk.Button(button_frame, text="Back", bg='#00A9FF', command=self.back_to_portfolio)
        back_button.pack(side=tk.RIGHT)

    def open_add_art_window(self):
        if self.history_window is None:
            self.history_window = History(self)
        else:
            self.history_window.lift()

    def back_to_portfolio(self):
        self.withdraw()  
        portfolio_window = PortfolioApp(self)
        portfolio_window.mainloop()


class History(tk.Toplevel):
    def __init__(self, master):
        super().__init__(master)

        self.title('Artist')
        self.attributes('-fullscreen', True)  
        self.bind("<Escape>", lambda event: self.attributes('-fullscreen', False))
        self.configure(bg='#89CFF3')
        
        main_frame = tk.Frame(self, width=280, height=230, bg='#CDF5FD')  
        main_frame.pack(fill=tk.BOTH, expand=True, padx=120, pady=50)
        
        label1 = tk.Label(main_frame, text='Name Of Artist:', font=('Arial', 12),bg='#CDF5FD')
        label1.grid(row=1, column=0, sticky='w', padx=5, pady=5)
        self.user_obj = tk.Entry(main_frame)
        self.user_obj.grid(row=1, column=1, sticky='we', padx=5, pady=5)
        
        label2 = tk.Label(main_frame, text='Price:', font=('Arial', 12),bg='#CDF5FD')
        label2.grid(row=3, column=0, sticky='w', padx=5, pady=5)
        self.user_pwd = tk.Entry(main_frame)
        self.user_pwd.grid(row=3, column=1, sticky='we', padx=5, pady=5)
        
        label3 = tk.Label(main_frame, text='Description:', font=('Arial', 12),bg='#CDF5FD')
        label3.grid(row=5, column=0, sticky='w', padx=5, pady=5)
        self.description_box = tk.Text(main_frame, height=5)
        self.description_box.grid(row=5, column=1, sticky='we', padx=5, pady=5)
        
        self.select_picture_button = tk.Button(main_frame, text='Select Picture', command=self.select_picture,bg='#00A9FF')
        self.select_picture_button.grid(row=7, column=0, columnspan=2, padx=5, pady=5)

        button_submit = tk.Button(main_frame, text='Submit', bg='#00A9FF', command=self.submit)
        button_submit.grid(row=9, column=0, columnspan=2, padx=5, pady=5)
        
        main_frame.columnconfigure(1, weight=1)
        self.file_name=None
        
    def select_picture(self):
        self.file_name = filedialog.askopenfilename(
            parent=self,
            title="Select Picture",
            filetypes=[("Image Files", "*.png;*.jpg;*.jpeg;*.bmp;*.gif")]
        )
        if self.file_name:
            print("Selected Picture:", self.file_name)
            
    def submit(self):
        artist_name = self.user_obj.get()
        price = self.user_pwd.get()
        description = self.description_box.get("1.0", tk.END)
        self.withdraw() 
        
        doc_ref = db.collection(u'art').add({
            u'artist_name': artist_name,
            u'price': price,
            u'description': description,
            u'image_path': self.file_name
        })
        
    def close_window(self):
        self.withdraw()
        self.master.deiconify()


if __name__ == '__main__':
    parent_window = PortfolioApp(tk.Tk())  
    window = ArtistInfo(parent=parent_window)
    window.mainloop()
