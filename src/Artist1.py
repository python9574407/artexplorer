
import tkinter as tk
from tkinter import filedialog
from PIL import Image, ImageTk
import firebase_admin
from firebase_admin import credentials, firestore

cred = credentials.Certificate("artexplorer\\artexplorer-fa168-firebase-adminsdk-q5zil-bb3fe1310e.json")  # Update with your service account key path
firebase_admin.initialize_app(cred)
db = firestore.client()

class PortfolioApp:
    def __init__(self, master):
        self.master = master
        self.master.title("artExplorer Portfolio")
        self.master.configure(bg="#89CFF3")
        
        self.header_frame = tk.Frame(self.master, bg="#DCBFFF") 
        self.header_frame.grid(row=0, column=0, columnspan=2, sticky="ew")
        
        self.header_label = tk.Label(self.header_frame, text="Art Explorer", bg="#DCBFFF", fg="#114303", font=("Segoe Script", 35))
        self.header_label.pack(padx=10, pady=10)
        
        self.canvas = tk.Canvas(self.master, width=40, height=40, bg="#DCBFFF", bd=0, highlightthickness=0)
        self.canvas.create_oval(5, 5, 35, 35, outline="black", fill="#00A9FF")
        self.canvas.grid(row=0, column=1, padx=10, pady=10, sticky="ne")
        self.canvas.bind("<Button-1>", self.toggle_slider)
        self.right_frame = tk.Frame(self.master, bg="#CDF5FD")
        self.right_frame.grid(row=1, column=0, padx=20, pady=20, sticky="nsew")
        
        self.master.rowconfigure(1, weight=1)
        self.master.columnconfigure(0, weight=1)
        
        self.slider_visible = False
        
        self.fetch_data()  
        self.create_widgets()  
        
    def fetch_data(self):
        self.art_data = [] 
        art_collection = db.collection('art').get()
        for doc in art_collection:
            art_info = doc.to_dict()
            self.art_data.append(art_info)
        
    def create_widgets(self):
        num_columns = 5
        for index, art_info in enumerate(self.art_data):
            row = index // num_columns
            col = index % num_columns
            art_frame = tk.Frame(self.right_frame, bd=2, relief=tk.RAISED, bg="white")
            art_frame.grid(row=row, column=col, padx=5, pady=5, sticky="nsew")
            
            
            image_path = art_info.get("image_path")
            if image_path:
                image = Image.open(image_path)
                image = image.resize((100, 100), Image.ANTIALIAS)  
                photo = ImageTk.PhotoImage(image)
                image_label = tk.Label(art_frame, image=photo, bg="white")
                image_label.image = photo
                image_label.grid(row=0, column=0, padx=5, pady=5)
            
     
            artist_label = tk.Label(art_frame, text=art_info.get("artist_name", "Unknown Artist"), bg="white")
            artist_label.grid(row=1, column=0, padx=5, pady=5)
         
            description_label = tk.Label(art_frame, text=art_info.get("description", "Art Description"), bg="white")
            description_label.grid(row=2, column=0, padx=5, pady=5)
            
           
            price_label = tk.Label(art_frame, text="Price: " + art_info.get("price", "Rs.XX.XX"), bg="white")
            price_label.grid(row=3, column=0, padx=5, pady=5)
            
            buy_button = tk.Button(art_frame, text="Buy", command=lambda idx=index: self.buy_art(idx), bg="#00A9FF")
            buy_button.grid(row=4, column=0, padx=5, pady=5)
            
        for i in range(row + 1):
            self.right_frame.grid_rowconfigure(i, weight=1)
        for j in range(num_columns):
            self.right_frame.grid_columnconfigure(j, weight=1)
    
    def toggle_slider(self, event):
        if self.slider_visible:
            self.hide_slider()
        else:
            self.show_slider()
        
    def show_slider(self):
        if not self.slider_visible:
            self.slider_frame = tk.Toplevel(self.master)
            self.slider_frame.geometry("150x100")
            self.slider_frame.overrideredirect(True)
            x = self.master.winfo_rootx() + self.canvas.winfo_x() + self.canvas.winfo_width() // 2 - 75
            y = self.master.winfo_rooty() + self.canvas.winfo_y() + self.canvas.winfo_height() + 5
            self.slider_frame.geometry(f"+{x}+{y}")
            buttons = [
                ("MyAccount", self.dummy_function, "#89CFF3"),     
                ("Be an Artist", self.dummy_function, "#89CFF3"),
                ("Logout", self.dummy_function, "#89CFF3")    
            ]
            for button_text, command, bg_color in buttons:
                tk.Button(self.slider_frame, text=button_text, command=command, bg=bg_color).pack(fill=tk.X)
            
            self.slider_visible = True
        
    def hide_slider(self):
        if self.slider_frame:
            self.slider_frame.destroy()
            self.slider_visible = False
        
    def dummy_function(self):
        pass
    
    def buy_art(self, index):
        art_info = self.art_data[index]
        
def main():
    root = tk.Tk()
    app = PortfolioApp(root)
    root.attributes('-fullscreen', True) 
    root.bind("<Escape>", lambda event: root.attributes('-fullscreen', False))  #
    root.mainloop()

if __name__ == "__main__":
    main()
